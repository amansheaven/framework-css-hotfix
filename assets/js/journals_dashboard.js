$(document).ready(function (e) {
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('[name="_csrfToken"]').val()
    }
  })
})

function populate_authors (authors, type) {
  var authors = authors.split(',')
  for (var i = 0; i < authors.length; i++) {
    $('#' + type.toLowerCase() + '_authors_input').addTag(authors[i])
  }
}

function addAuthorsNotAutoSuggested (author) {
  $.get('/admin/authors/search/' + author, function (data, status) {
    data = JSON.parse(data)
    if (data.length < 1) {
      addNewAuthor(author)
    }
  })
}

function addNewAuthor (author) {
  $.post('/admin/authors/add/' + author, function (data, status) {})
}

function add_tag_helper (author, search) {
  $('#cdlp_authors_input').addTag(author)
  $('#cdln_authors_input').addTag(author)
  $('#input-foot-tags-parent').html('')
  $('#cdlp_authors_input').removeTag(search)
  $('#cdln_authors_input').removeTag(search)
}

function populate_AuthorsSearch (name) {
  $.get('/admin/authors/search/' + name, function (data, status) {
    data = JSON.parse(data)
    limit = data.length <= 5 ? data.length : 5
    $('#input-foot-tags-parent').html('')
    for (var i = 0; i < limit; i++) {
      var authors = data[i].author
      authors = authors.split(',')
      for (var j = 0; j < authors.length; j++) {
        if (authors[j].toLowerCase().includes(name)) {
          $('#input-foot-tags-parent').append(
            '<span onclick="add_tag_helper(\'' +
              authors[j] +
              "','" +
              name +
              '\')" class="input-foot-tags">' +
              authors[j] +
              ' </span>'
          )
        }
      }
    }
  })
}

function remove_cdlp_pdf () {
  $('#uploadCDLPPdf').show()
  $('.ajax-file-upload-statusbar').hide()
}

function remove_cdln_pdf () {
  $('#uploadCDLNPdf').show()
  $('.ajax-file-upload-statusbar').hide()
}

function submit_cdlp () {
  var cdlp_title = $('input[name="cdlp_title"]').val()
  var cdlp_status = $('input[name="cdlp_status"]').val()
  var cdlp_pdf_link = $('input[name="cdlp_pdf_link"]').val()
  var authors = $('input[name="authors"]').val()
  if ([cdlp_title, cdlp_status, cdlp_pdf_link, authors].includes('')) {
    $('#cdlp_error').show()
    $('#cdlp_error').animate({ opacity: '.2' })
    $('#cdlp_error').animate({ opacity: '1' })
  } else {
    $('#form-cdlp').submit()
  }
}

function success_article_upload (link, TYPE) {
  $('.ajax-file-upload-container').show()
  $('#upload' + TYPE + 'Pdf').hide()

  $('#edit_' + TYPE.toLowerCase() + '_filename').html(link)
  $('.ajax-file-upload-red').html('delete')
  $('.ajax-file-upload-red').show()
  $('.ajax-file-upload-abort').attr(
    'onclick',
    'remove_' + TYPE.toLowerCase() + '_pdf()'
  )
  $('#' + TYPE.toLowerCase() + '_pdf_link').val(link)
}

function delete_article_show () {
  $('#delete_article_warning').show()
  $('#delete_article_button').hide()
}

function delete_article_close () {
  $('#delete_article_warning').hide()
  $('#delete_article_button').show()
}

function delete_article_confirm (id) {
  $.post('/admin/journals/delete/' + id, function (data, status) {
    window.location.href = '/admin/journals/add'
  })
}

function submit_cdln () {
  var cdln_title = $('input[name="cdln_title"]').val()
  var cdln_status = $('input[name="cdln_status"]').val()
  var cdln_pdf_link = $('input[name="cdln_pdf_link"]').val()
  var authors = $('input[name="authors"]').val()
  var notes_data = CKEDITOR.instances.cdln_editor.getData()
  $('#cdln_content').val(notes_data)
  if ([cdln_title, cdln_status, authors].includes('')) {
    $('#cdln_error').show()
    $('#cdln_error').animate({ opacity: '.2' })
    $('#cdln_error').animate({ opacity: '1' })
  } else {
    $('#form-cdln').submit()
  }
}

$(function () {
  $('#cdlp_authors_input').tagsInput({
    width: 'auto',
    defaultText: 'Add Author',
    onAddTag: addAuthorsNotAutoSuggested
  })

  $('#cdln_authors_input').tagsInput({
    width: 'auto',
    defaultText: 'Add Author',
    onAddTag: addAuthorsNotAutoSuggested
  })

  $('#cdlp_authors_input_tag').on('input', function (e) {
    var search = $('#cdlp_authors_input_tag').val()
    if (search.length > 2) {
      populate_AuthorsSearch(search)
    }
  })

  $('#cdln_authors_input_tag').on('input', function (e) {
    var search = $('#cdln_authors_input_tag').val()
    if (search.length > 2) {
      populate_AuthorsSearch(search)
    }
  })

  $('#uploadCDLPPdf').uploadFile({
    url: '/admin/articles/add/CDLP/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: 'pdf',
    acceptFiles: 'pdf',
    autoSubmit: true,
    maxFileSize: 5000 * 1024,
    onSuccess: function (files, data, xhr, pd) {
      success_article_upload(data, 'CDLP')
    }
  })

  $('#uploadCDLNPdf').uploadFile({
    url: '/admin/articles/add/CDLN/upload',
    multiple: false,
    maxFileCount: 1,
    allowedTypes: 'pdf',
    acceptFiles: 'pdf',
    autoSubmit: true,
    onSuccess: function (files, data, xhr, pd) {
      success_article_upload(data, 'CDLN')
    }
  })
})

var i = 0
function preview_loader () {
  $('#myProgress').animate({ opacity: '1' })
  $('#myBar').animate({ width: '0%' })
  $('#myBar').animate({ width: '100%' })
  $('#myProgress').animate({ opacity: '0' })
}

var renderEditorMath = {
  pArticleAuthors: document.getElementById('pArticleContent'),
  update: function (tex) {
    this.pArticleAuthors.innerHTML = tex
    MathJax.Hub.Queue(['Typeset', MathJax.Hub, this.formula])
  }
}

function toggle_cdln_preview (toggle) {
  if (toggle == 'show') {
    var title = $('input[name="cdln_title"]').val()
    var authors = $('input[name="cdln_authors"]').val()
    var notes_data = CKEDITOR.instances.cdln_editor.getData()
    authors = authors.split(',')
    $('#pArticleAuthors').html('')
    for (var j = 0; j < authors.length; j++) {
      if (j == authors.length - 1) {
        $('#pArticleAuthors').append(authors[j])
      } else {
        $('#pArticleAuthors').append(authors[j] + ', ')
      }
    }
    $('#pArticleName').html(title)
    renderEditorMath.update(notes_data)
    $('#articlePreviewDiv').show()
    $('#form-cdln').hide()
    $('#relatedActions').hide()
    $('#articleEditDiv')
      .removeClass('col-lg-7')
      .addClass('col-lg-2')
    $('#closePreviewButton').show()
    preview_loader()
  } else {
    $('#articlePreviewDiv').hide()
    $('#form-cdln').show()
    $('#relatedActions').show()
    $('#articleEditDiv')
      .removeClass('col-lg-2')
      .addClass('col-lg-7')
    $('#closePreviewButton').hide()
  }
  $('#articlePreviewDiv').animate({ opacity: '.2' })
  $('#articlePreviewDiv').animate({ opacity: '1' })
  $('#articleEditDiv').animate({ opacity: '.2' })
  $('#articleEditDiv').animate({ opacity: '1' })
}
